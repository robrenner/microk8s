# microk8s

Notes / Journal learning Microk8s.  

## Description

Below sequence of setup and deployment on a single node, ultimately behind a reverse proxy.  So deployed an environment on an old laptop behind a traefik reverse-proxy.

## Phase 1

Used official microk8s docs to craft an ansible role to deploy microk8s, install add-ons, and add my user to the microk8s group.

Researched via a number of articles, videos, & gits the k8s yaml structure, pod & deployments, namespaces.  Coinncides with my podman learning initiative.

Succussfully deployed microk8s to my target host.  Exposing the dashboard is still a learning exercise, currently doing it temporaly with ```port-forward``` cli command. There are a few ways to do this, need more research...

Wrote a pod yaml for containous/whoami container, which maps to host port 8080.  Deployed it using the dashboard, success.  Tested from host machine and remote machine, success.

Next up to test:
 * More complicated pod with storage requirements, environmental variables, 
 * Ingress; requires more learning.  Default nginx, or traefik?

So lets move on to storage test. Will lsio images be a problem with their user mapping?  If so we can go to the native images.  But what about Heimdall? It's a key test case.  So lets proceed with it. Developed console.yml and deployed.  Worked, wrote data to host directory, but seem to be issues.  Errors getting all data written. Some application data not available, some created files owned by root.  So is this a container issue or a volume issue?  In checking a bit more by starting a few more pods, Ombi among them, I see the issue is that my containers can't talk to the internet to get additional data.It's something to do with coredns, can see it in the logs. Need to figure out what ports to open for outbound access.
https://discuss.kubernetes.io/t/kubernetes-pods-do-not-have-internet-access/5252/5

Read this and follow. 
https://kubernetes.io/docs/concepts/configuration/overview/

The issue wasn't k8s, but microk8s. Host network/firewall settings, ```iptables```, wasn't accepting outbound traffic from microk8s.
https://microk8s.io/docs/troubleshooting#heading--common-issues
https://github.com/ubuntu/microk8s/issues/75

Needed to install iptables-persistent and run command ```sudo iptables -P FORWARD ACCEPT``` and reboot.  That got outbound traffic working.  Ubuntu server doesn't have iptables-persistant installed.  Will add these two items to the microk8s ansible role.

### Heimdall

linuxserver/heimdall has the following general docker config:
```
    docker create \
    --name=heimdall \
    -e PUID=1000 \
    -e PGID=1000 \
    -e TZ=Europe/London \
    -p 80:80 \
    -p 443:443 \
    -v /path/to/appdata/config:/config \
    --restart unless-stopped \
    linuxserver/heimdall
```
Might be a little challenge around https? nope.

## Phase 2

Also tried to deploy filebrowser with the volume connections to files, which was a problem.  Type ```FileorCreate``` wasn't recognized?  but also gave errors like it was a DNS path.  Deployed Ombi, and had the same issue as Heimdall, no outbound internet access.

Around this so much to learn in the next steps; services, host storage vs persistant volumes; ingress vs ingress controller vs CRDs.  In my reading caught a note that services should be created before the corresponding deployment, something to keep in mind.  Also been taking the whoami.yml and converting it from a pod to deployment file, good learning and good practice.

Ombi Helm Chart
https://github.com/billimek/billimek-charts/tree/master/charts/ombi

Egress
https://monzo.com/blog/controlling-outbound-traffic-from-kubernetes

### Service

In k8s Pods are temporary, created and die.  Deployments expand that concept to orchestrate them dynamically. Services provide a way to abstract the pods and provide a placeholder for the other pods and networking allocations.  The service is there, so other deployments don't have to care about the current state of the underlying deployment.

### Ingress

Ingress lets us take the requests at the host port/nic and forward them on to services.  Going to try with nginx controller.  
    internet
        |
   [ Ingress ]
   --|-----|--
   [ Services ]

https://kubernetes.io/docs/concepts/services-networking/ingress/
https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-ingress-guide-nginx-example.html

In microk8s, Ingress addon adds an NGINX Ingress Controller for MicroK8s. It is enabled by running the command:

```microk8s enable ingress```

https://microk8s.io/docs/addon-ingress

So we look through the samples above. Seems workable.  So things to work out on domain naming connventions.  These configuration examples leave questions though, they only look at Ingress to Service.  What about Ingress to the host?  Do they automaticlly expose the http ports?  What about TLD naming.  Must be further instructions on how to configure your Ingress Controller.

Microk8s docs denote the following 2 files for the nginx ingress controller:
  * nginx-ingress-tcp-microk8s-conf
  * nginx-ingress-udp-microk8s-conf

These config maps?  Quite possibly so.  And config maps may be how we configure the open queries above.  NetworkPolicy might be tied up in that as well.  Look at the latter parts of this link:  https://monzo.com/blog/controlling-outbound-traffic-from-kubernetes

### Storage

So our base install with the storage add-on lets us mount to host directories.  So technically covered.  If we wanted to be more correct to true production practices, we would use the storage/volume tools, such as persistent volumes.

When using a persistentvolumeclaim to a persistentvolume, within our pod volumeMounts we can declare a subpath.  So in theory could define a persistent volume to locate our container config files, and then use the subpath to define the specific container folder.  Does the PVC prevent othe pods from using the PV?
https://kubernetes.io/docs/concepts/storage/volumes/#using-subpath

Even if the shared config volume doesn't work, the PV would still be useful for our media directories.  Since on the PV/PVC we can define the file permissions and prevent file deletions.

## Phase 3

So at this point we've solved a number of challenges and learned a lot of mechanincs and practices.  Still need to learn the specifics of Ingress Controllers, but at this point think we're ready for another set of test deployments.  

Goals:
  * Deployments: cloudcmd, jellyfin, heimdall, whoami
  * expose dashboard to host port
  * create PVs to ds216 NFS shares for Jellyfin, and associated PVCs
  * Services for the deployments that map to host ports
  * Use a project namespace

To achieve the goals we perform the following actions:

  1. [x] Create a services yaml file for the 4 deployments
  2. [x] ~~Create PV yaml files for the (3) volumes~~ NFS mounts in deployment yaml's
  3. [x] Create the host directory structure for the deployment configs
  4. [x] Create the deployment yaml files
  5. [x] ~~Edit the dashboard yaml to expose hostPort~~
  6. [x] ~~Apply the PVs~~ Apply the secrets
  7. [x] Apply the services
  8. [x] Apply the deployments
  9. [x] Test the deployments
  10. [ ] Bonus: Traefik Helm Chart review
  11. [x] Bonus: Create secret

### Storage

So writing up PVs and PVCs, easy enough but don't think they are the right tool for serving up our NFS file library for deployment use.  The PVC appears to select a PV based upon available capacity, not be directly assigned.  So appears to be dynamic managed by k8s.  Can we direclty mount the PV into the pod?  Do we directly mount the NFS into the pod?

NFS volume may be the way to go.  https://kubernetes.io/docs/concepts/storage/volumes/#nfs

https://cloud.netapp.com/blog/kubernetes-nfs-two-quick-tutorials-cvo-blg
shows us how to mount the nfs share directly in the pod.

### Links

https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/readonlymany-disks
https://github.com/kubernetes/examples/tree/master/staging/volumes/nfs
https://gitlab.com/nanuchi/kubernetes-tutorial-series-youtube/-/tree/master/demo-kubernetes-components
https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-update-deployment

## Phase 4

### Networking

In this the networking and port exposure is tricky.  With single node should be relativly simple, but this is all engineered for clusters, and presumes typically some sort of external load balancer(?).

So for our ingress controller how do we expose and control the ports on our host.  Initial examples have exposed the ingress service on nodePort (30000+), but in that case how do we get http traffic to these atypical high port numbers.  Presumably requesting 443 as a nodePort will fail.

There is a hostNetwork function, which applies to Pods.  There is a hostPort function, which applies to containers.  NodePort applies to the k8s Service.  From link 
https://webcache.googleusercontent.com/search?q=cache:N0kHfihYLF4J:alesnosek.com/blog/2017/02/14/accessing-kubernetes-pods-from-outside-of-the-cluster/+&cd=1&hl=en&ct=clnk&gl=us&client=firefox-b-1-d 
there is a key note that if true may be a beneficial workaround.  "the nginx based Ingress controller is deployed as a set of containers running on top of Kubernetes. These containers are configured to use hostPorts 80 and 443 to allow the inbound traffic on these ports from the outside of the Kubernetes cluster."

So we have Ingress which acts as a k8s internal load balancer, and without an external load balancer, ingress would need defined on specific nodes/nodeIPs to expose hostPorts rather than nodePorts.

### Traefik

So I continue to try and figure out ingress.  I've got my head around the high level concept, but the mechanics of it seems complex.  Initial traefik research seemed complicated, so decided to look into using the add-on for nginx.  It seems just as complicated and have the learning curve of not having used it.  So back to Traefik.  A little searching and maybe we're making in roads.

So a big question is does Traefik do automatic service discovery in k8s like it does in Docker?

So a search for 'k8s traefik' on reddit yielded a comment thread with the following link.  
https://github.com/GoingOffRoading/For_Antebios  
This provides another significant data point as it has a Plex config utilizing PV/PVCs as well.

The reddit thread also yielded this github link: 
https://github.com/polds/k8s-traefik-certmanager 
Some good examples, note looks like using configmap to recreate/contain static toml file. Which is mounted on the deployment as a volume.  I follow most of what was done here, need to understand if the RBAC is needed.

This link gives a little more info and helps clarify rbac, single node cert, deployment namespace,
https://ralph.blog.imixs.com/2020/02/01/kubernetes-setup-traefik-2-1/ 
RBAC fives traefik ability for sercice detection.  Note also uses a serviceaccount, but also the kube-system namespace.  Also has the benefit of showing how the whoami deployment/service/ingress was set up.
Also note the yaml file naming scheme in these 3 examples, pretty sure they are using the numbering prefix to set an apply order on a 'wildcard' kubectl apply command.

The Traefik v1.7 guide also may shed some light.  Specifically look at the DaemonSet object, which exposes the ingress-controller via hostPort.
https://docs.traefik.io/v1.7/user-guide/kubernetes/

So for Traefik deployment we will need to create
  * Custom Resource Definitions (CRD)
  * RBAC (?)
  * ConfigMap (for static toml, how about for dynamic toml?)
  * ServiceAccount (? internal pod security feature, api access)
  * Service
  * Deployment

So need to compare the polds github to the ralph.blog, and should be able to develop a workable test framework.

For CRD and RBAC, they two contain basically the same thing.  Exact match on CRD.  For RBAC polds used a seperate file, included namespace references, and organized some resource lists a bit different, but all in all the same. Polds include a ServiceAccount is RBAC file, Ralph put it in his Service yaml.

I like polds yaml file organization better, Ralph has a tighter RBAC.  Go back and look at traefik docs on configuration discovery, & compare it against the yamls as well.

https://www.reddit.com/r/kubernetes/comments/hpdg7e/does_anybody_have_traefik_2_working_with_the/
https://docs.traefik.io/reference/dynamic-configuration/kubernetes-crd/

So tried a test deployment, everything worked & connected internally, but couldn't get to the pods from outside the host/k8s network.  So found this link about adding the security context.  
https://tech.evaneos.com/traefik-as-an-ingress-controller-on-minikube-with-kustomize-helm-a3b2f44a5c2a 
Still unsuccessful.  So searched against microk8s & traefik and saw this article.  
https://blog.dd19.de/blog/2019/11/15/traefik-2-0-on-microk8s/
Where he finds using it as a daemonset rather than a deployment works.  Which was referenced in the traefik v1.7 docs above.  So jumped right in and re-deployed, still getting 404s on the sites, but am seeing the Traefik Default Cert. So we're closer.

So before changing the traefik deployment, expiremented with changing the host address on whoami ingress from unms.lan/whoami to unms.lan, and success reached the pod via the url.  If memory serves had similar issues during the v1 to v2 conversion, in not understanding how to correctly write host names. But is only working for http requests, https still 404s out. Part of it may be combining both in 1 ingress and needing to seperate to two like examples.  Do I need to declare cert resolver on ingress? Applying the fix in this link worked for whoami; https://community.containo.us/t/404-help-please/4121/3 , and would further speak to why polds creates seperate ingress routes for http and https.  Verified other locations, as the TLS applies on a router, you cannot have only one IngressRoute to handle the 2 cases.

So to the TLS certificates. Can we use our dynamic.toml and host volume mounting to execute it the same as in docker?  Can we store the TLS key as a k8s secret and map to it that way? Or do you have to for our use case, everything is working fine with the traefik default cert, so unless we're going to add a cert validated by CA, even if self-signed, what is the point of the effort.

This post uses a self-signed cert w/ traefik v1.7 & k8s. Basically does what I do with docker, where the cert is mounted as a volume. He uses a config map for the toml, and stored the cert as a k8s secret.
https://medium.com/@patrickeasters/using-traefik-with-tls-on-kubernetes-cb67fb43a948

https://github.com/containous/traefik/tree/master/docs/content/user-guides/crd-acme

### Actions

So lets prep another round of test deployments. HTTP should be doable enough.  Need to figure out the certs for HTTPS.

* [x] Delete prior deployments
* [x] Develop namespace
* [x] Develop host DNS record {unms.lan}
* [x] Prep whoami deployment|service|ingress
* [x] Develop traefik crd
* [x] Develop traefik rbac
* [x] Develop traefik service
* [x] Develop traefik deployment
* [x] Develop traefik configmap
* [x] Deploy namespace
* [x] Deploy configmap
* [x] Deploy CRD & RBAC
* [x] Deploy traefik
* [x] Deploy whoami

## Next Step

With the test deployment working with traefik default certs we move on to next steps.  Investigating the network errors in the host logs and determining if they will be present on production host or if a function of the laptop host utilizing networkmanager.  Set up an ingress to the k8s dashboard.

## References

### Dashboard

https://virtualizationreview.com/articles/2019/01/30/microk8s-part-2-how-to-monitor-and-manage-kubernetes.aspx

To enable the dashboard and the DNS service, enter ```microk8s.enable dns dashboard ingress```.  After doing this, you can enter ```microk8s.kubectl get all``` to see that various services have been started.

As the K8s environment has a unique IP addressing scheme, you'll need to set up a proxy to pass requests to access the Dashboard, which you can do by entering ```microk8s.kubectl proxy --accept-hosts=.* --address=0.0.0.0 &```. Note that the ```&``` at the end of this command string will run the command in the background. 
Enter into web browser
http://{Ubuntu_IP_address}:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

### Deploy a Pod

https://virtualizationreview.com/articles/2019/02/01/microk8s-part-3-how-to-deploy-a-pod-in-kubernetes.aspx

Deploy nginx pod (2 pods in example),
```microk8s.kubectl run tjf-nginx --image=nginx:alpine --replicas=2 --port=80```
but what image registry is being used?
To expose pod ports to host external interface,
```microk8s.kubectl expose deployment tjf-nginx --type=NodePort --name=tjf2-nginx```
To check on external expose,
```microk8s.kubectl get service```

